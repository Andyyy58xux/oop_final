package mainProgram;

//Exception for failing to making an order.
public class OrderFailException extends Exception{
    public OrderFailException(String s){
        super(s);
    }
}
