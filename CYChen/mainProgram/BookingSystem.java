package mainProgram;

import java.io.IOException;

public class BookingSystem {

	public static void main(String[] args) throws ClassNotFoundException, IOException, OrderFailException {
		MainPage mp = new MainPage();
        mp.run();
	}

}
