package mainProgram;
import java.io.IOException;
import java.util.*;
import Database.*;
import Trip.TripOrder;
import Trip.TripPackage;



public class MainPage {
	//a constructor to initialize our database or something.
	public MainPage() throws ClassNotFoundException, IOException {
		BookingSystemDatabase.setUpDatabase();
	}

    //
    public void run() throws OrderFailException {
        //this is the loop for our main page.
    	Scanner scanner = new Scanner(System.in);
    	
    	System.out.println("Who are you?");
    	String userId = scanner.nextLine();
    	Account user = new Account(userId);
    	String _temp;
    	
        while(true){
            //item one: showing and searching for trip packages.
            //item two: modify an order.   
            //item three: search for an order.
        	
        	System.out.println("Type the command:\n1 for placing order.\n2 for looking up an order.\n3 for updating an order\n4 for deleting an order.\n5 for searching for a trip package.");
        	int cmd = scanner.nextInt();
		//consume newline.
		_temp = scanner.nextLine();
        	if(cmd == 1) {
        		System.out.print("Type the packageId you want to order: ");
        		int packageId = scanner.nextInt();
        		int[] numberOfPeople = new int[3];
        		System.out.print("Type the number of people for child, adult, elderly. ");
			for(int i=0;i<3;i++){
				
        			numberOfPeople[i] = scanner.nextInt();
			}
			//consume newline.
			_temp = scanner.nextLine();
        		TripOrder res = TripOrder.placeOrder(user, packageId, numberOfPeople);
        	}
        	else if(cmd == 2) {
        		System.out.print("Type the orderId you want to query: ");
        		int orderId = scanner.nextInt();
			//consume newline.
			_temp = scanner.nextLine();
        		TripOrder res = TripOrder.queryOrder(user, orderId);        		
        	}
        	else if(cmd == 3) {
        		System.out.print("Type the orderId you want to update: ");
        		int orderId = scanner.nextInt();
        		int[] numberOfPeople = new int[3];
        		System.out.print("Type the number of people for child, adult, elderly. ");
			for(int i=0;i<3;i++){
				
        			numberOfPeople[i] = scanner.nextInt();
			}
			//consume newline.
			_temp = scanner.nextLine();
			
        		TripOrder res = TripOrder.updateOrder(user, orderId, numberOfPeople);
        	}
        	else if(cmd == 4) {
        		System.out.print("Type the orderId you want to delete: ");
        		int orderId = scanner.nextInt();
			//consume newline.
			_temp = scanner.nextLine();
        		TripOrder.deleteOrder(user, orderId);
        	}
		else if(cmd == 5){
        		System.out.print("Enter the date you want to start after in XXXX-XX-XX format, e.g. 2020-05-20.");
			String date=scanner.nextLine();
			java.sql.Date startAfterDate=java.sql.Date.valueOf(date);
				
			System.out.println("Enter a the destination area");
			String area=scanner.nextLine();
			TripPackage tp=new TripPackage(area,startAfterDate);

			System.out.println("Enter price or date to specify whether results should be listed based on price or date.");
			String sort=scanner.nextLine();

                	//see exception in java file.
			try{
                		//make a query using tp.
				List<TripPackage> ltp=TripPackage.queryPackage(tp);

                    
                    		//by calling returnSorted on ltp we sort it based on the second parameter, which can be "date"(from recent to distant), or "price"(from low to high).
				TripPackage.returnSorted(ltp,sort);

				for(TripPackage itp:ltp){
					System.out.println("id: "+itp.getPackageId()+", title: "+itp.getTitle()+", area: "+itp.getArea()+", start date: "+itp.getStartDate()+", price: "+itp.getPrice());
					//break;

				}
			}catch(Exception e){
				System.out.println(e);	

			}


		}
        	else {
        		System.out.println("All orders in TripOrder table");
        		TripOrder.printOrders();
        	}
        }
    }
}
