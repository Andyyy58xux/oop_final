package Database;
import java.sql.*;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.io.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import java.io.FileReader;


public class BookingSystemDatabase {
	// Set up database and insert 
	public static void setUpDatabase() throws ClassNotFoundException, IOException {
		  // load the sqlite-JDBC driver using the current class loader
	      Class.forName("org.sqlite.JDBC");

	      Connection connection = null;

			






	      try {
	         // create a database connection	    	 
	         connection = DriverManager.getConnection("jdbc:sqlite:BookingSystemDatabase.db");

	         Statement statement = connection.createStatement();
	         statement.setQueryTimeout(5);  // set timeout to 30 sec.
	         
	         // create Account table
	         statement.executeUpdate("DROP TABLE IF EXISTS Account");
	         statement.executeUpdate("CREATE TABLE Account ("
	        		 + "userId STRING,"
	        		 + "PRIMARY KEY (userId))"
	        		 );

	         // create TripPackage table
	         statement.executeUpdate("DROP TABLE IF EXISTS TripPackage");
	         statement.executeUpdate("CREATE TABLE TripPackage ("
	         		+ "id INTEGER , "
	         		+ "title STRING,"
	         		+ "area STRING,"
	         		+ "travelCode INTEGER,"
	         		+ "productKey STRING,"
	         		+ "price INTEGER,"
	         		+ "startDate DATE,"
	         		+ "endDate DATE,"
	         		+ "lowerBound INTEGER,"
	         		+ "upperBound INTEGER,"
	         		+ "alreadySignedUp INTEGER,"
	         		+ "PRIMARY KEY (id))"
	         		);

	         // create TripOrder table
	         statement.executeUpdate("DROP TABLE IF EXISTS TripOrder");
	         statement.executeUpdate("CREATE TABLE TripOrder ("
	         		+ "id INTEGER,"
	         		+ "userId STRING,"
	         		+ "packageId INTEGER,"
	         		+ "price INTEGER,"
	         		+ "startDate DATE,"
	         		+ "endDate DATE,"
	         		+ "identities STRING,"
	         		+ "PRIMARY KEY (id),"
	         		+ "FOREIGN KEY (userId) REFERENCES Account(userId) ON DELETE CASCADE)"
	         		);
	         
	         // read trip_data_all.csv and insert to TripPackage database
	         try {


				  JSONParser parser = new JSONParser();
				  JSONArray areaJSONArray = (JSONArray)parser.parse(new FileReader("travel_code.json"));



				  Map<String,String> areaMap=new HashMap<String,String>();
				  for(Object areaObject: areaJSONArray){
				 		JSONObject areaJSONObject =(JSONObject) areaObject;
		
						areaMap.put((String)areaJSONObject.get("travel_code"),(String)areaJSONObject.get("travel_code_name"));	
		
		
				  }


	        	 InputStreamReader isr = new InputStreamReader(new FileInputStream("trip_data_all.csv")); //檔案讀取路徑



			     BufferedReader reader = new BufferedReader(isr);


	        	 String line = null;
	        	 	     
	        	 line = reader.readLine();
	        	 while((line = reader.readLine())!=null) {
	        		 String item[] = line.split(",");
	        		 // deal with the case with title "開春省很大♯旅展折$3,000－尼泊爾神仙國度．喜馬拉雅山脈．奇旺國家公園野性之旅9日(兩內陸段)(含稅簽)"
	        		 if(item.length == 9) {
	        			 String newItem[] = Arrays.copyOfRange(item, 1, item.length);
	        			 newItem[0] = item[0] + ',' + newItem[0];
	        			 item = Arrays.copyOfRange(newItem, 0, item.length);
	        		 }
	        		 else if(item.length < 8) {
	        			 continue;
	        		 }

	        		 Date startDate = Date.valueOf(item[4]);
	        		 Date endDate = Date.valueOf(item[5]);
	        		 PreparedStatement query = connection.prepareStatement("INSERT INTO TripPackage "
	        				 + "(title, area, travelCode, productKey, price, startDate, endDate, lowerBound, upperBound, alreadySignedUp)"
	        		 		 + "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
	        		 query.setString(1, item[0]);
					 if(areaMap.get(item[1])!=null) query.setString(2, areaMap.get(item[1]));
					 else{
						query.setString(2,"unknown");
						System.out.println("unknown area code encounter while building database.");
					}
	        		 query.setInt(3, Integer.parseInt(item[1]));
	        		 query.setString(4, item[2]);
	        		 query.setInt(5, Integer.parseInt(item[3]));
	        		 query.setDate(6, startDate);
	        		 query.setDate(7, endDate);
	        		 query.setInt(8, Integer.parseInt(item[6]));
	        		 query.setInt(9, Integer.parseInt(item[7]));
	        		 query.setInt(10, 0);
	        		 query.executeUpdate();
//        					 + "'" + item[0] + "'" + ','
//        					 + "'todo'"  + ','
//        					 + item[1] + ','
//        					 + "'" + item[2] + "'" + ','
//        					 + item[3] + ','
//        					 + "'" + item[4] + "'" + ','
//        					 + "'" + item[5] + "'" + ','
//        					 + item[6] + ','
//        					 + item[7] + ','
//        					 + "0);"
//        					 );
	        	 }
	        	 reader.close();

//	        	 Statement statement1 = connection.createStatement();
//	        	 ResultSet s1 = statement1.executeQuery("SELECT * from TripPackage");
//	        	 while(s1.next()) {
//					count++;
//					System.out.println(count);
//	        		 System.out.println("id = " + s1.getInt("id"));
//	        		 System.out.println("title = " + s1.getString("title"));
//	        		 System.out.println("area = " + s1.getString("area"));
//	        		 System.out.println("travelCode = " + s1.getInt("travelCode"));
//	        		 System.out.println("productKey = " + s1.getString("productKey"));
//	        		 System.out.println("price = " + s1.getInt("price"));
//	        		 System.out.println("startDate = " + s1.getDate("startDate"));
//	        		 System.out.println("endDate = " + s1.getDate("endDate"));
//	        		 System.out.println("lowerBound = " + s1.getInt("lowerBound"));
//	        		 System.out.println("upperBound = " + s1.getInt("upperBound"));
//	        		 System.out.println("alreadySignedUp = " + s1.getInt("alreadySignedUp"));
//	        	 }        	 
	         } catch(FileNotFoundException e) {
	        	 e.printStackTrace();
	         }catch(ParseException e){
				System.out.println(e);

			 }
	         

	      } catch(SQLException e) {
	    	  System.err.println(e.getMessage()); 
	      } finally {
	    	  try {
	              if(connection != null)
	                 connection.close();
              } catch(SQLException e) {  // Use SQLException class instead.          
            	  System.err.println(e); 
	          }
	      }
	}
	
	public static Connection getDatabaseConnection(){
		Connection connection = null;
		
		try {
//			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:BookingSystemDatabase.db");
			
		} catch(SQLException e) {
			System.err.println(e.getMessage());
		}	

		return connection;
	}
}
