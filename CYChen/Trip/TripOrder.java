package Trip;
import java.sql.*;
import java.util.Arrays;

import Database.BookingSystemDatabase;
import mainProgram.Account;
import mainProgram.OrderFailException;

public class TripOrder{
	// private
    // fields: orderID, Account, packageID, start_date, end_date, numberOfPeople[3]
    private int orderId;
    private Account user;
    private int packageId;
    private int price;
    private Date startDate;
    private Date endDate;
    private int[] numberOfPeople;
    
    // getter
    public int getOrderId() {
    	return this.orderId;
    }
    public Account getUser() {
    	return new Account(this.user.getUserId());
    }
    public int getPackageId() {
    	return this.packageId;
    }
    public int getPrice() {
    	return this.price;
    }
    public Date getStartDate() {
    	return (Date)this.startDate.clone();
    }
    public Date getEndDate() {
    	return (Date)this.endDate.clone();
    }
    public int[] getNumberOfPeople() {
    	return Arrays.copyOf(this.numberOfPeople, this.numberOfPeople.length);
    }
    
    // private constructor
    private TripOrder(int orderId, Account user, int packageId, int price, Date startDate, Date endDate, int[] numberOfPeople) {
    	this.orderId = orderId;
    	this.user = new Account(user.getUserId());
    	this.packageId = packageId;
    	this.price = price;
    	this.startDate = (Date)startDate.clone();
    	this.endDate = (Date)endDate.clone();
    	this.numberOfPeople = Arrays.copyOf(numberOfPeople, numberOfPeople.length);
    };
    
    /**
     * this is used to order a trip package. both order and package database should be updated if success. no modification to data on error, e.g. no package on the specified date, no spaces for that much people, invalid people etc.
    	calendar is a java build-in class.
    	modify both database table.
     * @param user
     * @param packageId
     * @param numberOfPeople
     * @return the inserted TripOrder object
     * @throws OrderFailException
     */
    public static TripOrder placeOrder(Account user, int packageId, int[] numberOfPeople) throws OrderFailException {
    	TripOrder queryResult = null;
    	Connection conn = null;
    	try {
    		// check if the numberOfPeople array all >= 0
    		for(int n:numberOfPeople) {
    			if(n < 0)
    				throw new OrderFailException("Please type valid numbers!");
    		}
    		
    		conn = BookingSystemDatabase.getDatabaseConnection();
    		// get information from TripPackage table
    		PreparedStatement query = conn.prepareStatement("SELECT * from TripPackage where id=?");
    		query.setInt(1, packageId);
    		ResultSet res = query.executeQuery();
    		int price = res.getInt("price");
    		Date startDate = res.getDate("startDate");
    		Date endDate = res.getDate("endDate");
    		int alreadySignedUp = res.getInt("alreadySignedUp");
    		System.err.printf("original alreadySignedUp = %d\n", alreadySignedUp);
			int upperBound = res.getInt("upperBound");
			int sum = Arrays.stream(numberOfPeople).sum();
			if(alreadySignedUp + sum > upperBound) {
				throw new OrderFailException("Insertion exceeds the upper bound of the package!");
			}
			
			// update the 'alreadySignedUp' field in TripPackage table
    		query = conn.prepareStatement("UPDATE TripPackage SET alreadySignedUp=? where id=?");
    		query.setInt(1, alreadySignedUp + sum);
			query.setInt(2, packageId);
			query.executeUpdate();
			
			// check the 'alreadySignedUp' field in TripPackage table
			query = conn.prepareStatement("SELECT * from TripPackage where id=?");
			query.setInt(1, packageId);
    		res = query.executeQuery();
    		alreadySignedUp = res.getInt("alreadySignedUp");
    		System.err.printf("updated alreadySignedUp = %d\n", alreadySignedUp);
    		
    		// insert an order into TripOrder table
    		query = conn.prepareStatement("INSERT INTO TripOrder "
    				+ "(userId, packageId, price, startDate, endDate, identities)"
    				+ "values(?, ?, ?, ?, ?, ?)");
    		query.setString(1, user.getUserId());
    		query.setInt(2, packageId);
    		query.setInt(3, price);
    		query.setDate(4, startDate);
    		query.setDate(5, endDate);
    		query.setString(6, Arrays.toString(numberOfPeople));
    		query.executeUpdate();
			
    		
    		// call constructor to get the object which will return later
    		query = conn.prepareStatement("SELECT * from TripOrder where id=(SELECT MAX(id) FROM TripOrder);");
    		res = query.executeQuery();
    		int orderId = res.getInt("id");
    		queryResult = new TripOrder(orderId, user, packageId, price, startDate, endDate, numberOfPeople);
    		
    		// test part
        	System.out.println(queryResult.orderId);
        	System.out.println(queryResult.user.getUserId());
        	System.out.println(queryResult.packageId);
        	System.out.println(queryResult.price);
        	System.out.println(queryResult.startDate);
        	System.out.println(queryResult.endDate);
        	for(int n:numberOfPeople) {
    			System.out.printf("%d ", n);
    		}
        	System.out.println();
    		
    	} catch(SQLException e) {
    		System.err.println(e.getMessage());
    	} catch (OrderFailException e) {
    		System.err.println(e.getMessage());
    	} finally {
    		try {
    			if(conn != null)
    				conn.close();
            } catch(SQLException e) {  // Use SQLException class instead.          
          	  System.err.println(e);
            }
    	}
    	
    	return queryResult;
    }
    
    /**
     * query an order. fails on situation like no such order exist. No null will be returned on success. read error message on error.
     * @param user
     * @param orderId
     * @return the queried TripOrder object
     * @throws OrderFailException
     */
    public static TripOrder queryOrder(Account user, int orderId) throws OrderFailException {
    	TripOrder queryResult = null;
    	Connection conn = null;
    	try {
    		conn = BookingSystemDatabase.getDatabaseConnection();
    		PreparedStatement query = conn.prepareStatement("SELECT (count(*) = 1) from TripOrder where userId=? AND id=?");
    		query.setString(1, user.getUserId());
    		query.setInt(2, orderId);
    		try (ResultSet rs = query.executeQuery()) {
    			if(rs.next()) {
    				boolean found = rs.getBoolean(1);
    				if(found) {
    					PreparedStatement queryInquire = conn.prepareStatement("SELECT * from TripOrder where userId=? AND id=?");
    					queryInquire.setString(1, user.getUserId());
    					queryInquire.setInt(2, orderId);
    					ResultSet res = queryInquire.executeQuery();
    					
    					int packageId = res.getInt("packageId");
    		    		int price = res.getInt("price");
    		    		Date startDate = res.getDate("startDate");
    		    		Date endDate = res.getDate("endDate");
    		    		String tmp = res.getString("identities");
    		    		String[] identities_str = tmp.substring(1, tmp.length()-1).split(",");
    		    		int[] numberOfPeople = new int[identities_str.length];
    		    		for(int i=0; i<identities_str.length; i++) {
    		    			numberOfPeople[i] = Integer.parseInt(identities_str[i].replaceAll("\\s+", ""));
    		    		}
    		    		
    		    		// test part
    		    		System.out.printf("packageId = %d\n", packageId);
    		    		System.out.printf("price = %d\n", price);
    		    		System.out.printf("startDate = %s\n", startDate.toString());
    		    		System.out.printf("endDate = %s\n", endDate.toString());
    		    		for(int n:numberOfPeople) {
    		    			System.out.printf("%d ", n);
    		    		}
    		    		System.out.println();
    		    		
    		    		// call constructor to get the object which will return later 
    		    		queryResult = new TripOrder(orderId, user, packageId, price, startDate, endDate, numberOfPeople);
    				}
    				else {
    					throw new OrderFailException("Query failed, (userId / orderId) does not exist!");
    				}
    			}
    		}
    		
    		
    	} catch(SQLException e) {
    		System.err.println(e.getMessage()); 
    	} catch (OrderFailException e) {
    		System.err.println(e.getMessage());
    	} finally {
    		try {
    			if(conn != null)
    				conn.close();
            } catch(SQLException e) {  // Use SQLException class instead.          
          	  System.err.println(e); 
            }
    	}
    	
    	return queryResult;
    }
    
    /**
     * change an order, which is to modify the number of people attending this trip in this order. Total number of people in array should not be 0, if you want to delete an order use TripOrder.deleteOrder().
    	error on too many people or no such order exist etc. read error message for reasons of error. No modification will be made to the database on error. New order information will be returned on success. No null will be returned on success.
     * @param user
     * @param orderId
     * @param numberOfPeople
     * @return the updated TripOrder object
     * @throws OrderFailException
     */
    public static TripOrder updateOrder(Account user,int orderId, int[] numberOfPeople) throws OrderFailException {
    	TripOrder queryResult = null;
    	Connection conn = null;
    	try {
    		// check if the numberOfPeople array all >= 0
    		for(int n:numberOfPeople) {
    			if(n < 0)
    				throw new OrderFailException("Please type valid numbers!");
    		}
    		
    		conn = BookingSystemDatabase.getDatabaseConnection();
    		// check if the order exists
    		PreparedStatement query = conn.prepareStatement("SELECT (count(*) = 1) from TripOrder where userId=? AND id=?");
    		query.setString(1, user.getUserId());
    		query.setInt(2, orderId);
    		try (ResultSet rs = query.executeQuery()) {
    			if(rs.next()) {
    				boolean found = rs.getBoolean(1);
    				if(found) {
    					PreparedStatement queryInquire = conn.prepareStatement("SELECT * from TripOrder where id=?");
    					queryInquire.setInt(1, orderId);
    					ResultSet res = queryInquire.executeQuery();
    					int packageId = res.getInt("packageId");
    					int price = res.getInt("price");
    		    		Date startDate = res.getDate("startDate");
    		    		Date endDate = res.getDate("endDate");
    		    		String tmp = res.getString("identities");
    		    		String[] identities_str = tmp.substring(1, tmp.length()-1).split(",");
    		    		int old_sum = 0;
    		    		for(int i=0; i<identities_str.length; i++) {
    		    			old_sum += Integer.parseInt(identities_str[i].replaceAll("\\s+", ""));
    		    		}
    		    		
    					queryInquire = conn.prepareStatement("SELECT * from TripPackage where id=?");
    					queryInquire.setInt(1, packageId);
    					res = queryInquire.executeQuery();
    					int alreadySignedUp = res.getInt("alreadySignedUp");
    					System.err.printf("original alreadySignedUp = %d\n", alreadySignedUp);
    					int upperBound = res.getInt("upperBound");
    					int new_sum = Arrays.stream(numberOfPeople).sum();
    					if(alreadySignedUp - old_sum + new_sum > upperBound) {
    						throw new OrderFailException("Modification exceeds the upper bound of the package!");
    					}
    					
    					// update the 'alreadySignedUp' field in TripPackage table if valid
    		    		query = conn.prepareStatement("UPDATE TripPackage SET alreadySignedUp=? where id=?");
    		    		query.setInt(1, alreadySignedUp - old_sum + new_sum);
    					query.setInt(2, packageId);
    					query.executeUpdate();
    					
    					// check the 'alreadySignedUp' field in TripPackage table
    					query = conn.prepareStatement("SELECT * from TripPackage where id=?");
    					query.setInt(1, packageId);
    		    		res = query.executeQuery();
    		    		alreadySignedUp = res.getInt("alreadySignedUp");
    		    		System.err.printf("updated alreadySignedUp = %d\n", alreadySignedUp);
    					
    					// update the 'identities' field in TripOrder table
    					PreparedStatement queryUpdate = conn.prepareStatement("UPDATE TripOrder SET identities=?  where id=?");
    					queryUpdate.setString(1, Arrays.toString(numberOfPeople));
    					queryUpdate.setInt(2, orderId);
    					queryUpdate.executeUpdate();
    					
    					// call constructor to get the object which will return later
    		    		queryResult = new TripOrder(orderId, user, packageId, price, startDate, endDate, numberOfPeople);
    		    		
    		    		// test part
    		    		System.out.printf("packageId = %d\n", queryResult.packageId);
    		    		System.out.printf("price = %d\n", queryResult.price);
    		    		System.out.printf("startDate = %s\n", queryResult.startDate.toString());
    		    		System.out.printf("endDate = %s\n", queryResult.endDate.toString());
    		    		for(int n:queryResult.numberOfPeople) {
    		    			System.out.printf("%d ", n);
    		    		}
    		    		System.out.println();
    				}
    				else {
    					throw new OrderFailException("Modification failed, orderId does not exist!");
    				}
    			}
    		}
    		
    	} catch(SQLException e) {    		
    		System.err.println(e.getMessage()); 
    	} catch (OrderFailException e) {
    		System.err.println(e.getMessage());
    	} finally {
    		try {
    			if(conn != null)
    				conn.close();
            } catch(SQLException e) {  // Use SQLException class instead.          
          	  System.err.println(e); 
            }
    	}
    	
    	return queryResult;
    }
    
    
    /**
     * delete an order. error on situation like order doesn't exist or too late to delete an order etc. read error message for reason of error.
     * @param user
     * @param orderId
     * @throws OrderFailException
     */
    public static void deleteOrder(Account user,int  orderId) throws OrderFailException {
    	Connection conn = null;
    	try {
    		conn = BookingSystemDatabase.getDatabaseConnection();
    		// check if the order exists
    		PreparedStatement query = conn.prepareStatement("SELECT (count(*) = 1) from TripOrder where userId=? AND id=?");
    		query.setString(1, user.getUserId());
    		query.setInt(2, orderId);
    		try (ResultSet rs = query.executeQuery()) {
    			if(rs.next()) {
    				boolean found = rs.getBoolean(1);
    				if(found) {
    					System.out.println("Order found!");
    					
    					PreparedStatement queryInquire = conn.prepareStatement("SELECT * from TripOrder where id=?");
    					queryInquire.setInt(1, orderId);
    					ResultSet res = queryInquire.executeQuery();
    					int packageId = res.getInt("packageId");
    					String tmp = res.getString("identities");
    					String[] identities_str = tmp.substring(1, tmp.length()-1).split(",");
    		    		int sum = 0;
    		    		for(int i=0; i<identities_str.length; i++) {
    		    			sum += Integer.parseInt(identities_str[i].replaceAll("\\s+", ""));
    		    		}
    					
    					query = conn.prepareStatement("SELECT * from TripPackage where id=?");
    					query.setInt(1, packageId);
    					res = query.executeQuery();
    					int alreadySignedUp = res.getInt("alreadySignedUp");
    					System.err.printf("original alreadySignedUp = %d\n", alreadySignedUp);
    					
    					// update the 'alreadySignedUp' field in TripPackage table if valid
    		    		query = conn.prepareStatement("UPDATE TripPackage SET alreadySignedUp=? where id=?");
    		    		query.setInt(1, alreadySignedUp - sum);
    					query.setInt(2, packageId);
    					query.executeUpdate();
    					
    					// check the 'alreadySignedUp' field in TripPackage table
    					query = conn.prepareStatement("SELECT * from TripPackage where id=?");
    					query.setInt(1, packageId);
    		    		res = query.executeQuery();
    		    		alreadySignedUp = res.getInt("alreadySignedUp");
    		    		System.err.printf("updated alreadySignedUp = %d\n", alreadySignedUp);
    		    		
    					PreparedStatement queryDelete = conn.prepareStatement("DELETE from TripOrder where userId=? AND id=?");
    					queryDelete.setString(1, user.getUserId());
    					queryDelete.setInt(2, orderId);
    					queryDelete.executeUpdate();
    					System.out.println("Deletion success!");
    				}
    				else {
    					throw new OrderFailException("Deletion failed, orderId does not exist!");
    				}
    			}
    		}
    		
    	} catch(SQLException e) {    		
    		System.err.println(e.getMessage()); 
    	} catch (OrderFailException e) {
    		System.err.println(e.getMessage());
    	} finally {
    		try {
    			if(conn != null)
    				conn.close();
            } catch(SQLException e) {  // Use SQLException class instead.          
          	  System.err.println(e); 
            }
    	}
    }

    /**
     * print out all orders in TripOrder table
     */
    public static void printOrders() {
    	try {
	    	Connection conn = BookingSystemDatabase.getDatabaseConnection();
			Statement statement = conn.createStatement();
			ResultSet s = statement.executeQuery("SELECT * from TripOrder;");
	       	while(s.next()) {
	       		 System.out.println("id = " + s.getInt("id"));
	       		 System.out.println("packageId = " + s.getInt("packageId"));
	       		 System.out.println("price = " + s.getInt("price"));
	       		 System.out.println("startDate = " + s.getDate("startDate"));
	       		 System.out.println("endDate = " + s.getDate("endDate"));
	       		 System.out.println("identities = " + s.getString("identities"));
	       		 System.out.println("-----------------");
	       	}
    	} catch(SQLException e) {    		
    		System.err.println(e.getMessage()); 
    	} catch (Exception e) {
    		e.printStackTrace();
    	} 
    }
}



