//this is a class used to represent a trip package. All the trip packages can be expressed as an array TripPackage[] or a collection like List<TripPackage>.

package Trip;
import Database.BookingSystemDatabase; 
import mainProgram.OrderFailException;
import java.sql.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Arrays;

public class TripPackage{


    //constructor. It is used to construct a TripPackage when making a query.
	//startAfterDate are inclusive. We are using java.sql.Date class.
    public TripPackage(String area,Date startAfterDate){
		this(PACKAGEID,TITLE,area,PRICE,startAfterDate,ENDDATE,PEOPLELOWERBOUND,PEOPLEUPPERBOUND,ALREADYSIGNEDUP);

	};
   

    //getter. 
    public int getPackageId(){
		return packageId;

	};
    public String getTitle(){
		return title;
	};
    public String getArea(){
		return area;
	};
    public int getPrice(){
		return price;

	};
    public Date getStartDate(){
		return (Date)startDate.clone();
	};
    public Date getEndDate(){
		return (Date)endDate.clone();

	};
    public int getPeopleLowerBound(){
		return peopleLowerBound;

	};
    public int getPeopleUpperBound(){
		return peopleUpperBound;

	};




    

    //return a collection of TripPackage satisfying the specified conditions.
    //no null will be returned, but size can be 0, which means no package meets the criteria. If there are some matches than the size is a positive integer. 

	//OrderFailException if tp is null or any of the two parameters in tp are null.
	//SQLException when database goes wrong, please retry. No modification happens on SQLException.
    public static List<TripPackage> queryPackage(TripPackage tp) throws OrderFailException, SQLException{


		if(tp==null) throw new OrderFailException("Buggy code: TripPackage shouldn't be null when making a query");

		if(tp.title==null || tp.startDate==null) throw new OrderFailException("Please provide both title and startdate when making a query.");


		PreparedStatement ps=DATABASECONNECT.prepareStatement("SELECT * from TripPackage where (area LIKE ?) and (startDate >= ?)");
		ps.setString(1,"%"+tp.getArea()+"%");
		ps.setDate(2,tp.getStartDate());

		ResultSet rs=ps.executeQuery();

		List<TripPackage> tpl=new ArrayList<TripPackage>();
		while(rs.next()){

			try{
				tpl.add(new TripPackage(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(6),rs.getDate(7),rs.getDate(8),rs.getInt(9),rs.getInt(10),rs.getInt(11)));
			}catch(SQLException e){
				System.out.println(e+" sql index error which shouldn't happen.");

			}	
		}
		
		return tpl;
	};
    
    
    //Sort the list tpl, passed as parameter, in a specified order.
	//The size of tpl may be 0 or a positive integer. String criteria can be "price", or "date".
    //this is a inplace sorting, which would modify the original list tpl.
	//if tpl is null, nothing would happen.
    public static void returnSorted(List<TripPackage> tpl, String criteria){

		if(tpl==null) return;

		Comparator<TripPackage> compare;
		if(criteria=="price") compare=new ComparatorPrice();
		else compare=new ComparatorDate();
				
		Collections.sort(tpl,compare);
		return;
	};
    
    
    
 
    
    //non-static fields:
    private int packageId;
    private String title;
    private String area;
    private int price;
    private Date startDate;
    private Date endDate;
    private int peopleLowerBound;
    private int peopleUpperBound;
    private int alreadySignedUp;

	//static constants.
    private static final int PACKAGEID;
    private static final String TITLE;
    private static final String AREA;
    private static final int PRICE;
    private static final Date STARTDATE;
    private static final Date ENDDATE;
	private static final int PEOPLELOWERBOUND;
	private static final int PEOPLEUPPERBOUND;
	private static final int ALREADYSIGNEDUP;
	private static final Connection DATABASECONNECT;//handler for database.

    //private constructor.
    private TripPackage(int packageId,String title,String area, int price, Date startDate, Date endDate, int peopleLowerBound,int peopleUpperBound,int alreadySignedUP){
		
    	this.packageId=packageId;
    	this.title=title;
    	this.area=area;
    	this.price=price;
		if(startDate==null) this.startDate=null;
    	else this.startDate=(Date)startDate.clone();
		if(endDate==null) this.endDate=null;
    	else this.endDate=(Date)endDate.clone();
    	this.peopleLowerBound=peopleLowerBound;
    	this.peopleUpperBound=peopleUpperBound;
    	this.alreadySignedUp=alreadySignedUp;

	};
    static{
    
    
		PACKAGEID=-1;
		TITLE="";
		AREA="";
		PRICE=-1;
		STARTDATE=Date.valueOf("1900-1-1");;
		ENDDATE=Date.valueOf("8000-12-31");
		PEOPLELOWERBOUND=-1;
		PEOPLEUPPERBOUND=-1;
		ALREADYSIGNEDUP=-1;
		DATABASECONNECT=BookingSystemDatabase.getDatabaseConnection();
    
    } 
}

class ComparatorDate implements Comparator<TripPackage>{
	public int compare(TripPackage t1,TripPackage t2){
		return (t1.getStartDate()).compareTo(t2.getStartDate());
	};
}

class ComparatorPrice implements Comparator<TripPackage>{
	public int compare(TripPackage t1,TripPackage t2){
		if(t1.getPrice()<t2.getPrice()) return -1;
		else if(t1.getPrice()==t2.getPrice()) return 0;
		else return 1;
	};
}
